#!/bin/sh
echo STAGE BUILD

apt -y update
apt -y install ruby-full build-essential zlib1g-dev git tzdata
ln -fs /usr/share/zoneinfo/Europe/Berlin /etc/localtime
dpkg-reconfigure --frontend noninteractive tzdata

apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash

apt-get -y update
apt-get -y install docker-ce docker-ce-cli containerd.io aufs-tools fuse fdutils \
    apparmor-profiles-extra apparmor-utils cgroupfs-mount bash-completion gitlab-runner

#curl -L "https://get.docker.com/" | bash

echo '# Install Ruby Gems to ~/gems' >> ~/.bashrc
echo 'export GEM_HOME="$HOME/gems"' >> ~/.bashrc
echo 'export PATH="$HOME/gems/bin:$PATH"' >> ~/.bashrc
source ~/.bashrc

gem install bundler -N
gem update --system -N

bundle install -j3

## build site
if [ ! -d "$JEKYLL_BUILDDIR" ]; then
    bundle exec jekyll new $JEKYLL_BUILDDIR
fi

cd $JEKYLL_BUILDDIR
bundle exec jekyll b --trace
cd ..

#docker -v
docker login $CI_REGISTRY -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD
gitlab-runner --help
#service docker restart
#dockerd -H unix:///var/run/docker.sock -H fd:// --storage-driver overlay2 &
#service docker status
#docker info
#docker run --rm -t gitlab/gitlab-runner --help
#docker login $CI_REGISTRY -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD
#ls -lh && pwd && ls -lh /builds/wryyyyyyyy/gl-starter-pack/_site/_site
#docker image ls
#docker container ls


#gitlab-runner register -n \
#  --url $CI_SERVER_URL/ \
#  --registration-token $CI_SERVER_TOKEN \
#  --executor shell \
#  --shell bash \
#  --description "My Runner"


#gitlab-runner register -n \
#  --url $CI_SERVER_URL/ \
#  --registration-token $CI_SERVER_TOKEN \
#  --executor docker \
#  --description "My Docker Runner" \
#  --docker-image "docker:latest" \
#  --docker-volumes /var/run/docker.sock:/var/run/docker.sock

#docker run --rm -t gitlab/gitlab-runner --help

#docker build -t registry.gitlab.com/wryyyyyyyy/gl-starter-pack:latest /builds/wryyyyyyyy/gl-starter-pack/_site/site
#docker image ls
#docker container ls

#docker build -t registry.gitlab.com/wryyyyyyyy/gl-starter-pack /builds/wryyyyyyyy/gl-starter-pack/_site

#curl -fsSL https://get.docker.com -o get-docker.sh
#bash get-docker.sh
#gitlab-runner --help
#gitlab-runner register -u $CI_SERVER_HOST -t $CI_SERVER_TOKEN
#docker run --rm -t gitlab/gitlab-runner --help
