#!/bin/sh
echo STAGE DEPLOY

apt-get -y update && apt -y upgrade
apt-get -y install git curl
apt -y install openssh-server

#echo '# Install Ruby Gems to ~/gems' >> ~/.bashrc
#echo 'export GEM_HOME="$HOME/gems"' >> ~/.bashrc
#echo 'export PATH="$HOME/gems/bin:$PATH"' >> ~/.bashrc
#source ~/.bashrc

#export JEKYLL_BUILDDIR="_site"

#gem install bundler -N
#gem update --system -N

#bundle install -j3

## build site
#if [ ! -d "$JEKYLL_BUILDDIR" ]; then
#    bundle exec jekyll new $JEKYLL_BUILDDIR
#fi

#cd $JEKYLL_BUILDDIR
#bundle exec jekyll b --trace
#cd ..

#cat /dev/zero | ssh-keygen -q -N ""

#cat /dev/zero | ssh-keygen -t ed25519 -C "${GITLAB_USER_EMAIL}" -q -N ""ls -lah ~/.ssh
#eval "$(ssh-agent -s)"
#ssh-add ~/.ssh/id_ed25519

#echo 'gitlab.com ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTjquxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf' >> ~/.ssh/known_hosts
#echo 'gitlab.com ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsj2bNKTBSpIYDEGk9KxsGh3mySTRgMtXL583qmBpzeQ+jqCMRgBqB98u3z++J1sKlXHWfM9dyhSevkMwSbhoR8XIq/U0tCNyokEi/ueaBMCvbcTHhO7FcwzY92WK4Yt0aGROY5qX2UKSeOvuP4D6TPqKF1onrSzH9bx9XUf2lEdWT/ia1NEKjunUqu1xOB/StKDHMoX4/OKyIzuS0q/T1zOATthvasJFoPrAjkohTyaDUz2LN5JoH839hViyEG82yB+MjcFV5MU3N1l1QL3cVUCh93xSaua1N85qivl+siMkPGbO5xR/En4iEY6K2XPASUEMaieWVNTRCtJ4S8H+9' >> ~/.ssh/known_hosts
#echo 'gitlab.com ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=' >> ~/.ssh/known_hosts



git config --global user.name ${GITLAB_USER_NAME}
git config --global user.email ${GITLAB_USER_EMAIL}
curl -X POST -F "private_token=${K8S_SECRET_GITLAB_DEPLOY_TOKEN}" -F "title=$(hostname)" -F "key=$(cat ~/.ssh/id_ed25519.pub)" "https://gitlab.com/api/v4/user/keys"

#git remote set-url origin "git@$CI_SERVER_HOST/$CI_PROJECT_PATH.git" # K8S_SECRET_GITLAB_DEPLOY_TOKEN
#git remote add origin git@github.com:${GITLAB_USER_NAME}/${CI_PROJECT_NAME}.git
#git remote -v
git checkout develop
rm -f deploy_timelog.txt
echo "${CI_COMMIT_TIMESTAMP}" >deploy_timelog.txt
git fetch
git pull
git add .
git commit -m "commited by $CI_DEPLOY_USER"
git push -o ci-skip "https://${GITLAB_USER_NAME}:${K8S_SECRET_GITLAB_DEPLOY_TOKEN}@$CI_SERVER_HOST/$CI_PROJECT_PATH.git" HEAD:develop
#git push --set-upstream origin develop

#CURRENTDATE="$(date)"
#echo "$CURRENTDATE added a line" | tee -a timelog.log
#git add .
#git commit -m "${CI_COMMIT_TITLE}-${CI_COMMIT_TIMESTAMP}"
#git push -o ci-skip git@github.com:${GITLAB_USER_NAME}/${CI_PROJECT_NAME}.git HEAD:develop

#git push --set-upstream origin develop
#git config --global user.name $GITLAB_USER_NAME
#git config --global user.email $GITLAB_USER_EMAIL
#ssh-add -K ~/.ssh/$DEPLOY_GITLAB_KEY
#ssh-add -K ~/.ssh/$DEPLOY_RSA_KEY
#ssh-add -K ~/.ssh/$GITLAB_DEPLOY_KEY
#ssh -T git@gitlab.com
#git checkout -B develop
#CURRENTDATE="$(date)"
#echo "$CURRENTDATE added a line" | tee -a timelog.log
#git fetch
#git status
#git add .
#git commit -m "[ci skip] updated timelog.log at $CURRENTDATE"
#git push --set-upstream origin develop
#git push -o ci-skip https://gitlab+deploy-token-331941:$DEPLOY_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git HEAD:develop
#git config --global user.password $CI_DEPLOY_PASSWORD
#git checkout -B develop
#git add .
#git commit -m "commited by $CI_DEPLOY_USER"
#docker login -u gitlab-ci-runner -p $gitlab-deploy-token
#git tag "v$(date +%s)"
#git push -o ci-skip https://root:$ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git HEAD:master
#git push --set-upstream origin develop
